import Data from "./mock.json";

const Project = ({
  thumbnail,
  name,
  description,
  stack,
  weblink,
  sourcecode,
}) => (
  <div className="m-2">
    <img src={thumbnail} alt={`thumbnail of ${name}`} height="300" />
  </div>
);

/**
 * @todo - add work for button/switch based filters rather than querystring based.
 */
const Filters = () => (
  <div className="d-flex justify-content-between py-4" style={{ width: 200 }}>
    <div>Website</div>
    <div>Webapp</div>
    <div>App</div>
  </div>
);

const Work = () => (
  <section className="px-4">
    <Filters />
    <div className="d-flex flex-wrap">
      {Data.projects.map((project, id) => (
        <Project key={id} {...project} />
      ))}
    </div>
  </section>
);

export default Work;
