/* eslint-disable no-restricted-globals */
const CACHE = "vaibhav";

const APP_RESOURCES = [
  "/",
  "/favicon.ico",
  "/manifest.json",
  "/images/icon32.png",
  "/images/icon64.png",
  "/images/icon192.png",
  "/images/icon512.png",
];

const LIBRARY_RESOURCES = [
  "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css",
  "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js",
];

const RESOURCES = [...APP_RESOURCES, ...LIBRARY_RESOURCES];

self.addEventListener("install", (e) =>
  e.waitUntil(caches.open(CACHE).then((cache) => cache.addAll(RESOURCES)))
);

self.addEventListener("fetch", (e) =>
  e.respondWith(
    caches.match(e.request).then((response) => response || fetch(e.request))
  )
);
